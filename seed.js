var seeder = require('mongoose-seed');
var User = require('./models/user');

// Connect to MongoDB via Mongoose

// //======= DATABASE SETTINGS
// var db_name = 'myappdatabase'
var db_name = 'gifts'
//provide a sensible default for local development
mongodb_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;
//take advantage of openshift env vars when available:
if (process.env.OPENSHIFT_MONGODB_DB_URL) {
	mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

// mongodb://$OPENSHIFT_MONGODB_DB_HOST:$OPENSHIFT_MONGODB_DB_PORT/
// mongodb://admin:VFd4PiraTYxM@/gifts

// DB setup
// mongoose.connect('mongodb://localhost/gifts');
// seeder.connect(mongodb_connection_string);
seeder.connect(mongodb_connection_string, function () {

	// Load Mongoose models
	seeder.loadModels([
		'./models/user',
		'./models/image'
	]);

	// Clear specified collections
	seeder.clearModels(['User', 'Image'], function () {

		// Callback to populate DB once collections have been cleared
		seeder.populateModels(data);

	});
});

// Data array containing seed data - documents organized by Model
var newUser = new User();

var data = [
	{
		'model': 'User',
		'documents': [
			{
				'local.email': 'marysia',
				'local.password': newUser.generateHash('kokos'),
				'role': 'admin'
			},
			{
				'local.email': 'maciek',
				'local.password': newUser.generateHash('kokos'),
				'role': 'admin'
			}
		]
	}
];
