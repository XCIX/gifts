var fs = require('fs'),
    path = require('path'),
    multer = require('multer'),
    Models = require('../models'),
    upload = multer({dest: path.join(__dirname, '../public/upload/temp')}).single('file');

module.exports = {

  index: function(req, res) {
  res.send('The image:index GET controller');
  },

  create: function(req, res) {

    upload(req,res,function(err) {
      if(err)
        return res.end("Error uploading file.");

      var saveImage = function() {
        var possible = 'abcdefghijklmnopqrstuvwxyz0123456789',
            imgUrl = '';

        for(var i=0; i < 6; i+=1) {
          imgUrl += possible.charAt(Math.floor(Math.random()*possible.length));
        }
        Models.Image.find({ filename: imgUrl }, function(err, images) {
          if (images.length> 0) {
            saveImage();
          } else {
            var tempPath = req.file.path;
            var ext = path.extname(req.file.originalname).toLowerCase();
            var targetPath = path.resolve('./public/upload/' + imgUrl + ext);
            if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif') {
              fs.rename(tempPath, targetPath, function(err) {
                if (err) throw err;
                var newImg = new Models.Image({
                  title: req.body.title,
                  link: req.body.link,
                  filename: imgUrl + ext
                });
                newImg.save(function(err) {
                  if (err) {
                    res.json(err);
                  } else {
                    res.redirect('/presents');
                  }
                });
              });

            } else {
              fs.unlink(tempPath, function () {
                if (err) throw err;
                res.json(500, {error: 'Only image files are allowed.'});
              });
            }
          }
        });
      };

      saveImage();
    });

  },

  take: function(req, res) {

    Models.Image.findOne({ filename: { $regex: req.params.image_id }},
      function(err, image) {
        if (!err && image) {
          image.taken = true;
          if (req.user) image._taken_by = req.user._id;
          image.save(function(err) {
            if (err) {
              res.json(err);
            } else {
              res.redirect('/presents');
            }
          });
        }
    });

  },

  untake: function(req, res) {

    Models.Image.findOne({ filename: { $regex: req.params.image_id }},
      function(err, image) {
        if (!err && image) {
          image.taken = false;
          image.save(function(err) {
            if (err) {
              res.json(err);
            } else {
              res.redirect('/presents');
            }
          });
        }
    });
  },

  edit: function(req, res) {

    upload(req,res,function(err) {
      if(err)
        return res.end("Error uploading file.");

      var saveImage = function() {
        var possible = 'abcdefghijklmnopqrstuvwxyz0123456789',
            imgUrl = '';

        for(var i=0; i < 6; i+=1) {
          imgUrl += possible.charAt(Math.floor(Math.random()*possible.length));
        }
        Models.Image.find({ filename: imgUrl }, function(err, images) {
          if (images.length> 0) {
            saveImage();
          } else {
            var tempPath = (req.file === undefined) ? '' : req.file.path;
            var ext =  (req.file === undefined) ? '' : path.extname(req.file.originalname).toLowerCase();
            var targetPath = path.resolve('./public/upload/' + imgUrl + ext);

            if (req.file != undefined) {
              console.log(req.body);
              if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif' || ext === '') {
                fs.rename(tempPath, targetPath, function(err) {
                  if (err) throw err;
                  console.log(req.params.image_id)
                  Models.Image.findOne({ filename: { $regex: req.params.image_id }},
                    function(err, image) {
                      if (!err && image) {
                        if (req.body.title) image.title = req.body.title;
                        if (req.body.link) image.link = req.body.link;
                        if (req.file) image.filename = imgUrl + ext;
                        image.save(function(err) {
                          if (err) {
                            res.json(err);
                          } else {
                            res.redirect('/presents');
                          }
                        });
                      }
                  });
                });

              } else {
                fs.unlink(tempPath, function () {
                  if (err) throw err;
                  res.json(500, {error: 'Only image files are allowed.'});
                });
              }
            } else {
            Models.Image.findOne({ filename: { $regex: req.params.image_id }},
              function(err, image) {
                if (!err && image) {
                  if (req.body.title) image.title = req.body.title;
                  if (req.body.link) image.link = req.body.link;
                  image.save(function(err) {
                    if (err) {
                      res.json(err);
                    } else {
                      res.redirect('/presents');
                    }
                  });
                }
              });
            }

          }
        });
      };

      saveImage();
    });

  },

  delete: function(req, res) {
    Models.Image.findOne({ filename: { $regex: req.params.image_id }},
      function(err, image) {
        if (!err && image) {
          image.remove(function(err) {
            if (err) {
              res.json(err);
            } else {
              res.redirect('/presents');
            }
          });
        }
    });
  }

};
