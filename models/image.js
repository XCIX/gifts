var mongoose = require('mongoose'),
path = require('path');

var ImageSchema = mongoose.Schema({
  _taken_by:    { type: String, ref: 'User' },
  title:        {type: String},
  filename:     {type: String},
  link:         {type: String},
  taken:        {type: Boolean, 'default': false},
  timestamp:    {type: Date, 'default': Date.now}
});

ImageSchema.virtual('uniqueId')
  .get(function() {
    return this.filename.replace(path.extname(this.filename), '');
  });

module.exports = mongoose.model('Image', ImageSchema);
