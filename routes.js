module.exports = function(app, passport) {

  var express   = require('express');
  var home      = require('./controllers/home');
  var image     = require('./controllers/image');
  // =====================================
  // HOME PAGE (with login links) ========
  // =====================================
  /* GET home page. */
  app.get('/', home.index);
  app.get('/presents', home.presents);

  // =====================================
  // LOGIN ===============================
  // =====================================
  // show the login form
  app.get('/login', function(req, res) {

      // render the page and pass in any flash data if it exists
      res.render('login.jade', { message: req.flash('loginMessage') });
  });

  // process the login form
  app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/presents', // redirect to the secure profile section
    failureRedirect : '/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));

  // =====================================
  // SIGNUP ==============================
  // =====================================
  // show the signup form
  app.get('/signup', function(req, res) {

      // render the page and pass in any flash data if it exists
      res.render('signup.jade', { message: req.flash('signupMessage') });
  });

  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/presents', // redirect to the secure profile section
    failureRedirect : '/signup', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));
  // =====================================
  // PROFILE SECTION =====================
  // =====================================
  // we will want this protected so you have to be logged in to visit
  // we will use route middleware to verify this (the isLoggedIn function)
  app.get('/profile', isLoggedIn, function(req, res) {
      res.render('profile.jade', {
          user : req.user // get the user out of session and pass to template
      });
  });

  // =====================================
  // LOGOUT ==============================
  // =====================================
  app.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/');
  });



  /* GET images listing. */
  // router.get('/:image_id', image.index);
  app.get('/images', image.index);

  app.get('/images/:image_id', image.index);

  // router.get('/images', home.index);


  app.post('/images/', image.create);

  app.post('/images/:image_id/take', image.take);
  app.post('/images/:image_id/untake', image.untake);
  app.post('/images/:image_id/delete', image.delete);

  // Tu do dodania co konkretnie jest edytowane
  app.post('/images/:image_id/edit', image.edit);

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

  // if user is authenticated in the session, carry on
  if (req.isAuthenticated())
      return next();

  // if they aren't redirect them to the home page
  res.redirect('/');
}
