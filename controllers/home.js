ImageModel = require('../models').Image;

module.exports = {

  index: function(req, res) {
    res.render('index');
  },

  presents: function(req, res) {

    isUserLogged = function (req) {
      return req.user != undefined;
    }

    isAdmin = function (req) {
      return isUserLogged(req) && req.user.role == 'admin';
    }

    var viewModel = {
      images: [],
      isUser: isUserLogged(req),
      isAdmin: isAdmin(req),
      user: req.user // Tu leci cały obiekt bo i tak się przydaje
    };

    ImageModel.find({}, {}, { sort: { timestamp: -1 }},

      function(err, images) {
        if (err) { throw err; }
        viewModel.images = images;
        res.render('presents', viewModel);
        // console.log('viewModel: ');
        // console.log(viewModel);
      });
  }


};
